DROP DATABASE IF EXISTS exercise;
CREATE DATABASE exercise;
USE exercise;

SELECT 'Create a table named "testing_table" with following fields: name (string), contact_name (string), roll_no (string)' AS Step1;

CREATE TABLE testing_table (
  name VARCHAR(20),
  contact_name VARCHAR(20),
  roll_no VARCHAR(10)
);

DESCRIBE testing_table;

SELECT 'Delete column name, rename contact_name to username. Add two fields first_name, last_name. Also change the type of roll_no to integer' AS Step2;

ALTER TABLE testing_table
  DROP COLUMN name,
  CHANGE COLUMN contact_name username VARCHAR(20),
  CHANGE COLUMN roll_no roll_no INT,
  ADD COLUMN first_name VARCHAR(20),
  ADD COLUMN last_name VARCHAR(20);

DESCRIBE testing_table;